<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asiannew');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V,*7lg#R7b4kdRWb [o]e+E6:?$# zPf9Sw>puHA!glQ[2AHPq1d!ChXaVkr6J=N');
define('SECURE_AUTH_KEY',  's89!5K7hVeG5^n1{|Add4/tTrP{j;.Frj)d{iWu2wMVa_jtW}`0w/kQfmYvNJpPN');
define('LOGGED_IN_KEY',    'p-t)Lf4@GOMJJNRHN`@2j&Z@onrY!V_MEB9:<lR#|q@9<SEy|y#>Ofyo(5{zk/:?');
define('NONCE_KEY',        '7i[ 9f|i)@v_F*>}qfMxt{S<-&!|rp~hcuQA.:.:iR4Gs2$!t`$#@k8a[7u/gZmw');
define('AUTH_SALT',        '(PdC&T3kj9^Wo^QFKm]~@|yNl`gVSB}[{m=3Kri90K27cM9T!ijcW4[(m+iC+M`7');
define('SECURE_AUTH_SALT', 'W*BC=24N5d)Xqfjk7)Mb)%QDoJX^b-<fWfLm(C_XF OF:A(ZR$rxDS-lHw:}I:lV');
define('LOGGED_IN_SALT',   'mh(cQ|_^Y)p!4 V0EOE,`sV8`K;WouC9vC2r?5>.oy$W-7c:%J,,yE+z<1DPjzhy');
define('NONCE_SALT',       ';ZjB/^IN~?&,7GmwfYA0*SFV;O7/6xX5{k>QBWfo>UWxK?gq4K_d&@H^VH|]Zya+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
