<!--------Header Start---------->
<div class="mainloader" > 
    <div class="loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<header>
    
        <nav class="navbar navbar-expand-xl">
<div class="container">
            <?php $mainImage = get_field('main_logo','options');?>
            
            <a class="navbar-brand" href="<?php echo get_site_url();?>"><img src="<?php echo $mainImage['url'];?>" alt="logo.png" /></a>

            <?php $stickyImage = get_field('sticky_logo','options');?>
            
            <a class="navbar_brand_skicky" href="<?php echo get_site_url();?>"><img src="<?php echo $stickyImage['url'];?>" alt="logo.png" /></a>
            
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            
                <span class="navbar-toggler-icon"></span>

            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <?php 
                        $menu = wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'navbar-nav') ); 
                ?>

            </div>
 </div>
        </nav>

   

</header>
