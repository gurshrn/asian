<?php 
/*
Template Name: contact_us
*/
get_header();
get_sidebar();

?>
    <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>



    

    <section class="contact_us_main">

      <div class="container">

          <div class="row">

            <div class="col-lg-5 contact_us_lt">

               <div class="contact_us_com contact_us_adress about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="500"  data-aos-duration="1000">

                 <h3><?php the_field('title');?></h3>

                   <p><?php the_field('sub_title');?></p>

                   
 
                   <div class="adress_bx">

                     <h3><?php the_field('address_title');?></h3>

                        <ul>
                            <li><span class="fa_icon_bx"><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php the_field('address','options');?></li>

                            <li><span class="fa_icon_bx"><i class="fa fa-phone" aria-hidden="true"></i></span><a href="javascript:void(0)"><?php the_field('phone_number','options');?></a></li>

                            <li><span class="fa_icon_bx"><i class="fa fa-envelope-o" aria-hidden="true"></i></span><a href="javascript:void(0)"><?php the_field('email','options');?></a></li>
                        </ul>

                   </div>

                </div>

              </div>

            <div class="col-lg-7 contact_us_rt">

              <div class="contact_form" data-aos="fade-left" data-aos-easing="ease" data-aos-delay="500"  data-aos-duration="1000">

                

                  <h3><?php the_field('contact_form_title');?></h3>

                    <!--div class="contact_form_inn"-->

                      <?php echo do_shortcode( '[contact-form-7 id="279" title="Contact form 1"]' ); ?>

                     

                        

                    <!--/div-->  

                  
              </div>

              </div>

              

          </div>

        </div>

    </section>

    



    

    <section class="manegement_team">

      <div class="container">

          <div class="about_text management_top text-center"  data-aos="fade-up" data-aos-easing="ease" data-aos-delay="500"  data-aos-duration="1000">

            <h3  data-aos="zoom-out" data-aos-easing="ease" data-aos-delay="500"  data-aos-duration="1000"><?php the_field('team_title','options');?></h3>

              <p><?php the_field('team_description','options');?></p>

          </div>

          <div class="row manegement_temm_detail management_tem_bottom">


            <?php 
                while( have_rows('about_team','options') ): the_row(); 

                $title = get_sub_field('about_team_title','options');
                $subTitle = get_sub_field('about_team_sub_title','options');
                $email = get_sub_field('about_team_email','options');
                $description = get_sub_field('about_team_description','options');
                $image = get_sub_field('about_team_image','options');

            ?>

             <div class="col-lg-6  manege_team_bx"  data-aos="fade-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                  <div class="row">

                    <div class="col-lg-6 col-md-4">

                        <figure class="manegement_team_pro" style="background-image: url(<?php echo $image['url'];?>)"></figure>

                      </div>

                      <div class="col-lg-6 col-md-8">

                          <div class="about_text">

                           <h3><?php echo $title;?></h3>

                              <p><?php echo $subTitle;?></p>

                              <p class="red_color"><?php echo $email;?></p>
                             <div class="custom_scroll ">
                               <p><?php echo $description;?></p>
							  </div>

                          </div>

                      </div>

                 </div>

              </div>

             <?php endwhile; ?>
            </div>

        </div>

    </section>

<?php get_footer(); ?>

