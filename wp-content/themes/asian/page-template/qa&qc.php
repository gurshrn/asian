<?php 
/*
Template Name: qa_qc
*/
get_header();
get_sidebar();

?>
     
    <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>

       

<section class="product_com qa_main_block">

    <div class="container">

        <div class="qa_qc_com certificate_block">

      <div class="section_head">

         <h2><?php the_field('certificate_main_title');?></h2>

        </div>

        <div class="row">

          <?php 
                while( have_rows('upload_certificate') ): the_row(); 

                $cerTitle = get_sub_field('certificate_title');
                $cerImage = get_sub_field('certificate_image');
          ?>

                <div class="col-lg-3 col-md-3 col-sm-6">

                   <div class="our_project_bx qa_certificate product_range border_product">

                      <figure class="product_figure" style="background-image:url(<?php echo $cerImage['url'];?>)"><a href="<?php echo $cerImage['url'];?>" target="_blank"></a></figure>

                      <h3><a href="<?php echo $cerImage['url'];?>" target="_blank"><?php echo $cerTitle;?></a></h3>

                    </div>

                  </div>

            <?php endwhile; ?>
          
          </div>

        </div>    

        

  <div class="qa_qc_com document_block">

      <div class="section_head">

         <h2><?php the_field('document_main_title');?></h2>

        </div>

        

        <div class="row">

          <?php 
                while( have_rows('documents') ): the_row(); 

                $docTitle = get_sub_field('document_title');
                $docImage = get_sub_field('document_image');
                $docPdf = get_sub_field('document_pdf');
          ?>

                <div class="col-lg-3 col-md-3 col-sm-6">

                   <div class="our_project_bx qa_certificate  document_product product_range border_product">

                      <figure class="product_figure" style="background-image:url(<?php echo $docImage['url'];?>)"></figure>

                       <a href="<?php echo $docPdf['url'];?>" class="download_bttn" target="_blank"><?php the_field('download_btn_text');?></a>

                      <h3><a href="<?php echo $docPdf['url'];?>" target="_blank"><?php echo $docTitle;?></a></h3>

                    </div>

                  </div>

            <?php endwhile; ?>

          </div>

        </div>

        

         <div class="qa_qc_com inspection_block">

      <div class="section_head">

         <h2><strong><?php the_field('inspection_title');?></strong></h2>

        </div>

        

        <div class="row">

            <?php 
              while( have_rows('tools') ): the_row(); 

              $title = get_sub_field('title');
              $description = get_sub_field('description');
              $image = get_sub_field('image');

            ?>

                <div class="col-lg-4 col-md-6">

                   <div class="inspection_bx">

                     <figure class="product_figure">

                       <img src="<?php echo $image['url'];?>" alt="inspection_product_1.png" />

                       </figure>

                       <div class="inspection_text">

                         <h3><?php echo $title;?></h3>

                           <p><?php echo $description;?></p>

                       </div>

                    </div>

                </div>

            <?php endwhile; ?>

           </div>

        </div>

    </div>

</section>  

<?php get_footer();?>

