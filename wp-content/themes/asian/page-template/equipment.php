<?php 
/*
Template Name: equipment
*/
get_header();
get_sidebar();

?>
    <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>

       

    <section class="quipment_block">

      <div class="container"> 

          <h2><strong><?php the_field('equipment_title');?></strong></h2>

          <?php 
                while( have_rows('machine_list') ): the_row(); 

                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $image = get_sub_field('image');

          ?>

          

                <div class="row quipment_row">

                  <div class="col-md-5 quipment_col_lt" data-aos="fade-right"  data-aos-delay="500"  data-aos-duration="1000">

                    <figure class="figure_bx equiment_figure" style="background-image: url(<?php echo $image['url'];?>)"></figure>

                    </div>

                     <div class="col-md-7 quipment_col_rt">

                         <div class="equiment_text equiment_border  about_text" data-aos="fade-right"  data-aos-delay="500"  data-aos-duration="1000">

                              <h3><?php echo $title;?></h3>

                              <p><?php echo $description;?></p>

                         </div>

                    </div>

                </div>

            <?php endwhile; ?>

        </div>

    </section>

<?php get_footer(); ?>

