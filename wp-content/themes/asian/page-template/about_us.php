<?php 
/*
Template Name: about_us
*/
get_header();
get_sidebar();

?>

    <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>

    <section class="about_compny">

      <div class="container">

          <div class="row">

              <div class="col-lg-7  about_left"> 

                  <div class="about_comn about_text custom_list" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="600"  data-aos-duration="1000">

                      <h3 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000"><?php the_field('intro_title');?></h3>

                      <p><?php the_field('intro_description');?></p>
                  </div>

              </div>

               <div class="col-lg-5 about_right">

                  <div class="about_comn about_banner"  data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <div class="skew_bx">

                        <span class="line line-left"></span>

                        <span class="line line-left-bottom"></span>

                        <?php $introImage = get_field('intro_image'); ?>

                         <figure class="figure_border" style="background-image: url(<?php echo $introImage['url'];?>)"></figure>

                        <span class="line line-right"></span>

                        <span class="line line-right-top"></span>

                      </div>

                  </div>

              </div>

          </div>

        </div>

    </section>

    <section class="about_compny philosophy_section">

      <?php $philoImage = get_field('philo_image'); ?>

      <div class="col-lg-5 philosophy_lt" style="background-image:url(<?php echo $philoImage['url'];?>)">

      </div>

      <div class="col-lg-7 philosophy_rt">

           <div class="about_text custom_list"   data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

            <h3><?php the_field('philo_title');?></h3>

               <p><?php the_field('philo_description');?></p>

          </div>

    </div>

</section>  

    <section class="about_compny satisfied_customer ">

      <div class="container">

          <div class="row">

              <div class="col-lg-7  about_left">

                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="600"  data-aos-duration="1000">

                      <h3 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000"><?php the_field('customer_title');?></h3>

                      <p><?php the_field('customer_description');?></p>

                  </div>

              </div>

               <div class="col-lg-5 about_right">

                  <div class="about_comn about_banner"  data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <div class="skew_bx">

                        <span class="line line-left"></span>

                        <span class="line line-left-bottom"></span>

                        <?php $customerImage = get_field('customer_image'); ?>

                         <figure class="figure_border" style="background-image: url(<?php echo $customerImage['url'];?>)"></figure>

                        <span class="line line-right"></span>

                        <span class="line line-right-top"></span>

                      </div>

                  </div>

              </div>

          </div>

        </div>

    </section>

    

    <section class="manegement_team">

      <div class="container">

          <div class="about_text management_top text-center"  data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

            <h3  data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000"><?php the_field('team_title');?></h3>

              <p><?php the_field('team_description');?></p>

          </div>

          <div class="row manegement_temm_detail management_tem_bottom">


            <?php 
                while( have_rows('about_team','options') ): the_row(); 

                $title = get_sub_field('about_team_title','options');
                $subTitle = get_sub_field('about_team_sub_title','options');
                $email = get_sub_field('about_team_email','options');
                $description = get_sub_field('about_team_description','options');
                $image = get_sub_field('about_team_image','options');

            ?>

             <div class="col-lg-6  manege_team_bx"  data-aos="fade-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                  <div class="row">

                    <div class="col-lg-6 col-md-4">

                        <figure class="manegement_team_pro" style="background-image: url(<?php echo $image['url'];?>)"></figure>

                      </div>

                      <div class="col-lg-6 col-md-8">
 
                          <div class="about_text">

                           <h3><?php echo $title;?></h3>

                              <p><?php echo $subTitle;?></p>

                              <p class="red_color"><?php echo $email;?></p>
                               <div class="custom_scroll">
                                 <p><?php echo $description;?></p>
                                  </div>
                          </div>

                      </div>

                 </div>

              </div>

             <?php endwhile; ?>
            </div>

          

        </div>

    </section>

<?php get_footer(); ?>

        

