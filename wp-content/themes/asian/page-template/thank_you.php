<!doctype html>

<html>

<head>

<meta charset="utf-8">

<title>Enimat | Thank You</title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">

<meta http-equiv="x-ua-compatible" content="ie=edge">

<!-- Favicon -->

<link rel="icon" href="favicon.ico" type="image/x-icon">



<!-- CSS -->

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/main.css">  

</head>

<body> 



   <div class="mainloader" > 

  <div class="loader">

      <span></span>

      <span></span>

      <span></span>

      <span></span>

    </div>

    </div>

    

        <section class="thank_main_section" style="background-image: url(assets/images/thank_you.jpg)">

            <div class="container">

              <div class="thank_you_inner">

                  <h1><span class="wow bounceInDown" data-wow-duration="0.5s" data-wow-delay="1s">T</span><span class="wow bounceInDown" data-wow-duration="0.7s"  data-wow-delay="1.1s">h</span><span class="wow bounceInDown" data-wow-duration="0.10s"  data-wow-delay="1.2s">a</span><span class="wow bounceInDown" data-wow-duration="0.12s"  data-wow-delay="1.3s">n</span><span class="wow bounceInDown" data-wow-duration="0.14s" data-wow-delay="1.4s">k</span> <span class="wow bounceInDown" data-wow-duration="0.16s" data-wow-delay="1.5s">y</span><span class="wow bounceInDown" data-wow-duration="0.18s" data-wow-delay="1.6s"><i class="fa fa-smile-o wow bounceInDown" aria-hidden="true" data-wow-duration="1.5s" data-wow-delay="2s"></i></span><span class="wow bounceInDown" data-wow-duration="0.22s" data-wow-delay="1.7s">u</span></h1>

                    <p class="wow fadeInUp animated " data-wow-duration="1.5s" data-wow-delay="1.5s">Your message has been successfully sent. We will contact you soon!</p>

            <!--      <h3 class="wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="1.5s">For more information</h3>

                    <p class="wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="1.5s">you can write to us at <a href="mailto:info@massagesconcierge.com">info@massagesconcierge.com</a> </p>-->

                 <!-- <p class="wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="1.5s">call at<a href="tel:18007790237"> 1-800-666-1524</a> </p>-->

                    <a href="index.html" class="btn golden-btn large-btn wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="1.5s">go to home</a>

                </div>

            </div> 

        </section>

    

    

    

 

<!-- jQuery first, then Bootstrap JS. -->

        <script src="assets/js/jquery.min.js"></script>

        <script src="assets/js/popper.min.js"></script>

        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/bootstrap-select.min.js"></script>

        <script src="assets/js/owl.carousel.js"></script>

        <script src="assets/js/wow.js"></script>

        <script src="assets/js/aos.js"></script>

        <script src="assets/js/custom.js"></script>

</body>

</html>

