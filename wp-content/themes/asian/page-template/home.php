<?php 
/*
Template Name: home
*/
get_header();
get_sidebar();
global $post;
?>

    <section class="banner homepage_banner">

      <div id="demo" class="carousel slide" data-ride="carousel">
          <!-- Indicators --> 
          <ul class="carousel-indicators">

            <?php
                $i=0; 
                while( have_rows('home_slider') ): the_row();

                   if($i == 0)
                   {
                    $active = 'active';
                //   }
                //   else
                //   {
                //     $active = '';
                //   }

            ?>

                <li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php echo $active;?>"></li>

            <?php } $i++; endwhile; wp_reset_query(); ?>

          </ul>

  

          <!-- The slideshow -->

          <div class="carousel-inner">
		  
            <?php 
                $i = 0;
                while( have_rows('home_slider') ): the_row(); 

                $hometitle = get_sub_field('slider_title');
                $homedesc = get_sub_field('slider_description');
                $videourl = get_sub_field('video_url');
                $btnText = get_sub_field('button_text');
                $btnLink = get_sub_field('button_link');
                if($i == 0)
                {
                  $active = 'active';
                // }
                // else
                // {
                //   $active = '';
                // }

            ?>

            <div class="carousel-item <?php echo $active;?>">
			
				<video width="100%" height="100%" src="<?php echo $videourl;?>" controls autoplay muted loop></video>


                 <!--video class="play-video<?php //echo $i;?>" width="560" height="315" src="<?php //echo $videourl.'&autoplay=1';?>"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></video-->

                <div class="carousel-item-overlay">

                  <div class="container">

                      <div class="banner_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="1000"  data-aos-duration="600">

                        <h1><?php echo $hometitle;?></h1>

                          <p><?php echo $homedesc;?></p>

                          <a href="<?php echo $btnLink;?>" class="btn red-btn banner_btn large_btn"><?php echo $btnText;?></a>
                          <a href="javascript:void(0);" data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="1500"  data-aos-duration="600" class="play_btn" data-rel="<?php echo $i;?>" id="videoPly"><i class="fa fa-play" aria-hidden="true"></i></a>

                      </div>

                      

                    </div>

                </div>

            </div>

           <?php } $i++; endwhile; wp_reset_query(); ?>

          </div>

        </div>

    </section>

    <section class="about_us_main">

      <div class="container">

          <div class="row">

              <div class="col-lg-7  about_left">

                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">

                      <h2 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="300"  data-aos-duration="1000"><strong><?php the_field('about_title');?></strong></h2>

                      <p><?php the_field('about_description');?></p>

                      <a href="<?php the_field('about_us_btn_link');?>" class="btn red-btn"><?php the_field('about_us_btn_text');?></a>

                  </div>

              </div>

               <div class="col-lg-5  about_right">

                  <div class="about_comn about_banner"  data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">

                      <div class="skew_bx">

                        <span class="line line-left"></span>

                        <span class="line line-left-bottom"></span>


                          <?php $aboutImage = get_field('about_image'); ?>

                         <figure class="figure_border" style="background-image: url(<?php echo $aboutImage['url'];?>)"></figure>

                        <span class="line line-right"></span>

                        <span class="line line-right-top"></span>

                      </div>

                  </div>

              </div>

          </div>
          
           <div class="row">

              <div class="col-lg-12 col-md-12 about_bottom_cont">

                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">

                      <p><?php the_field('contact_us_information');?></p>

                  </div>

              </div>
          </div>

        </div>
	

    </section>

    

    

    <section class="our_project_main">

      <div class="container">

          <div class="section_head">

            <h2 data-aos="zoom-out" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="500"><strong><?php the_field('product_title');?></strong></h2>

            </div>

          <div class="our_project_inn">

              <div class="loop owl-carousel owl-theme">

                <?php
                  $args = array(
                    'post_type'   => 'product',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                  );
                  $products = new WP_Query( $args );
                  if( $products->have_posts() ) :
                  ?>
                    
                      <?php
                        while( $products->have_posts() ) :
                          $products->the_post();
                        ?>
                            <div class="item">

                                <div class="our_project_bx">

                                    <figure class="product_figure" style="background-image:url(<?php echo the_post_thumbnail_url();?>)"><a href="#"></a></figure>

                                    <h3><a href="#"><?php echo  get_the_title(); ?></a></h3>

                                </div>

                             </div>
                            
                          <?php
                        endwhile;
                        wp_reset_postdata();
                      ?>
                   
                  <?php endif; ?>

                  
              </div>
          </div>

        </div> 

    </section>

    

    <section class="product_bottm_main">

      <div class="container">

          <div class="row">

              <?php 
                  while( have_rows('product_service') ): the_row(); 

                    $productSerTitle = get_sub_field('product_service_title');
                    $productSerDesc = get_sub_field('product_service_description');
                    $productSerImage = get_sub_field('product_service_image');

              ?>

                    <div class="col-md-4">

                         <div class="product_service_bx" data-aos="fade-right" data-aos-offset="100" data-aos-easing="ease-in-sine" data-aos-delay="300"  data-aos-duration="1000">

                            <figure style="background-image: url(<?php echo $productSerImage['url'];?>)"></figure>

                              <h3><?php echo $productSerTitle;?></h3>

                              <p><?php echo $productSerDesc;?></p>

                          </div>

                    </div>

                <?php endwhile; wp_reset_query(); ?>
             
            </div>
        
        </div>

    </section>

  
<section class="product_bottm_content_sec">
   	<video width="100%" height="100%" class="bk_ground_vedio"src="<?php the_field('video_url','options');?>" controls autoplay muted loop></video>
    <div class="video_content">
<div class="container">
     <div class="row">
              <div class="col-lg-12 col-md-12 product_bottm_content">
                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">
                      <p><?php the_field('product_service_description');?></p>
                  </div>
              </div>
          </div>
    </div>
 </div>        
</section> 
    

    

    

    <section class="blog_main" style="background-image: url('assets/images/blog_bg.jpg')">

      <div class="container">

          <div class="section_head">

            <h2 data-aos="zoom-out" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000"><?php the_field('blog_title');?><strong></strong></h2>

            </div>

          <div class="row blog_inn">

            <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );

                $post_query = new WP_Query($args);
                if($post_query->have_posts() ) 
                {
                  while($post_query->have_posts() ): 
                      $post_query->the_post();
            ?>
                      <div class="col-md-4">

                        <div class="blog_bx" data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                            <figure class="blog_figure" style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>

                            <div class="blog_bx_content">

                                 <div class="calandar_tag">

                                  <h4><strong><?php echo get_the_date('d'); ?></strong><?php echo get_the_date('M'); ?></h4>

                                </div>

                              <h3><b><?php the_title(); ?></b></h3>

                                <p><?php the_excerpt(); ?></p>

                                <a href="<?php the_permalink(); ?>" class="link">Read More...</a>

                            </div>

                        </div>

                      </div>
                    <?php
                  endwhile;
				  wp_reset_query();
                }
            ?>

            

                

                

          <div class="col-sm-12 blog_btn_outr button_outr text-center">

              <a href="<?php the_field('view_more_btn_link');?>" class="btn red-btn"><?php the_field('blog_view_btn_text');?></a>
	
              </div>

          </div>

        </div> 

    </section> 

    
<?php get_footer(); ?>

