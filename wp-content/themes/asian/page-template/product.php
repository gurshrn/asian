<?php 
/*
Template Name: product
*/
get_header();
get_sidebar();

?> 

    <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>

       

<section class="product_com produt_main">

    <div class="container">

          <div class="row">

              <div class="col-lg-12 col-md-12 product_top_cont">

                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">

                      <p><?php the_field('product_title');?></p>

                  </div>

              </div>


          </div>

    

        <div class="row">

          <?php
            $args = array(
              'post_type'   => 'product',
              'post_status' => 'publish',
			  'posts_per_page' => -1
            );
            $products = new WP_Query( $args );
            if( $products->have_posts() ) :
            ?>
              
                <?php
                  while( $products->have_posts() ) :
                    $products->the_post();
                  ?>
                      <div class="col-lg-3 col-md-4">

                       <div class="our_project_bx product_range border_product">

                          <figure class="product_figure" style="background-image:url(<?php echo the_post_thumbnail_url();?>)"><a href="<?php the_permalink(); ?>"></a></figure>

                          <h3><a href="<?php the_permalink(); ?>"><?php echo  get_the_title(); ?></a></h3>

                        </div>

                      </div>
                      
                    <?php
                  endwhile;
                  wp_reset_postdata();
                ?>
             
            <?php endif; ?>
        </div>
		
		 <div class="row">

              <div class="col-lg-12 col-md-12 product_bottom_cont">

                  <div class="about_comn about_text" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="600">

                      <h3><strong><?php the_field('product_end_title');?></strong></h3>

                      <p><?php the_field('product_end_description');?></p>

                  </div>

              </div>

               

          </div>

    </div>


    

    

</section>  

<?php get_footer(); ?>
