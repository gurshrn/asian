<?php 
/*
Template Name: our_customer
*/
get_header();
get_sidebar();

?>
  <?php $bannerImage = get_field('banner_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title');?></h1>

                 </div>

            </div>

    </section>

       

    

    <section class="our_customer our_globle_customer">

        <div class="container">

            <h3><?php the_field('title');?></h3>

            <div class="row">

                <?php 

                    $images = get_field('gallery');
                    //echo "<pre>"; print_r($images);
                    $size = 'full'; // (thumbnail, medium, large, full or custom size)

                    if( $images ): ?>
                        <?php foreach( $images as $image ): ?>

                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> 

                                  <figure class="customer_figuer"><img src="<?php echo $image['url']; ?>" alt="customer_img_1.png" /></figure>

                                </div>
                                
                            <?php endforeach; ?>
                        
                    <?php endif; 
                ?>
            </div>

       </div>        

    </section>

    

    

    <section class="our_customer customer_review">

         <div class="container">

            <h3><?php the_field('quote_title');?></h3>

              <div class="row">

                  <?php 
                      while( have_rows('customers_quote') ): the_row(); 

                      $quoteMessage = get_sub_field('quote_message');
                      $quoteImage = get_sub_field('quote_image');

                  ?>

                      <div class="col-md-4 col-sm-6">

                          <div class="review_bx">

                            <figure><img src="<?php echo $quoteImage['url'];?>" alt="quote-icon.png" /></figure>

                              <p><?php echo $quoteMessage;?></p>

                          </div>

                       </div>

                  <?php endwhile; ?>
            </div>

    </section>

   
<?php get_footer(); ?>

