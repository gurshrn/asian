<?php 
/*
Template Name: blog
*/
get_header();
get_sidebar();

?>

    <?php $blogImage = get_field('blog_image'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $blogImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('blog_title');?></h1>

                 </div>

            </div>

    </section>

    

   <section class="blog_main_block border_blog_bx">

        <div class="container">

            <div class="row">

              <?php
                $args = array(
                    'post_type' => 'post'
                );

                $post_query = new WP_Query($args);
                if($post_query->have_posts() ) 
                {
                  while($post_query->have_posts() ) 
                  {
                      $post_query->the_post();
              ?>

                <div class="col-md-4 col-sm-6">

                <div class="blog_bx" data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                    <figure class="blog_figure" style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>

                    <div class="blog_bx_content">

                         <div class="calandar_tag">

                          <h4><strong><?php echo get_the_date('d'); ?></strong><?php echo get_the_date('M'); ?></h4>

                        </div>

                      <h3><?php the_title(); ?></h3>

                        <p><?php the_excerpt(); ?></p>

                        <a href="<?php the_permalink(); ?>" class="link">Read More...</a>

                    </div>

                </div>

              </div>

            <?php } } ?>
                <!-- <div class="col-sm-12 pagination_bx">

                  <ul>

                    <li><a href="#" class="active">1</a></li>

                    <li><a href="#">2</a></li>

                    <li><a href="#">3</a></li>

                    <li><a href="#">4</a></li>

                    </ul>

                </div> -->

                

            </div>

       </div>

    </section>

<?php get_footer();?>
