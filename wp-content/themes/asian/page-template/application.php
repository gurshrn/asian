<?php 
/*
Template Name: application
*/
get_header();
get_sidebar();

?> 

  <?php $bannerImage = get_field('application_banner'); ?>

  <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('application_title');?></h1>

                 </div>

            </div>

    </section>

    

    <section class="application_block">

      <div class="container">

          <div class="row">

          <?php 
              while( have_rows('applications') ): the_row(); 

              $title = get_sub_field('title');
              $description = get_sub_field('description');
              $image = get_sub_field('image');

          ?>

           <div class="col-md-4" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

              <div class="appliction_bx">

                  <div class="appliction_detail">

                     <figure class="appliction_bx" style="background-image:url(<?php echo $image['url'];?>)"></figure>

                      <div class="text_overlay">

                          <p><?php echo $description;?></p>

                      </div>

                    </div>

                  <h3><?php echo $title;?></h3>

               </div>

           </div>

          <?php endwhile; ?>
          
        </div>

        </div>

    </section>

<?php get_footer(); ?>

   
