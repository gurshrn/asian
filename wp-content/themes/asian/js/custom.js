(function($) {
  'use strict';

  // Testimonials
  // var testimonials = $('#testimonials');
  // if (testimonials.length > 0) {
  //   testimonials.owlCarousel({
  //     loop: true,
  //     margin: 0,
  //     items: 1,
  //     nav: false,
  //     dots: true
  //   })
  // } 


  //Avoid pinch zoom on iOS 
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault(); 
    }
  }, false);
})(jQuery)


//owl 2 js
    var owl = jQuery('.our_project_main .owl-carousel');
      owl.owlCarousel({
            margin: 20,
            loop: false,
            nav: false,
            dots:true,
          autoplay:true,
          rewind:true,
        responsive: {
          0: {
            items: 1
          }, 
		  480: {
			  items: 2
		  },
          768 : {
            items: 3
          },
         
          1000: {
            items: 4
          }
        }
      })


jQuery(window).on('scroll' , function(){
 var headerHeight = jQuery(".banner").outerHeight(); 
       
    if(jQuery(window).scrollTop() >= headerHeight)
        {
            jQuery('header').addClass('animated fadeInDown sticky');
            jQuery('header .navbar').addClass('fadeInDown');

        } 
    else{
         jQuery('header').removeClass('animated fadeInDown sticky');
         jQuery('header .navbar').removeClass('fadeInDown sticky');
        
}
    });
    
    


 new WOW().init(); 

  AOS.init(); 

AOS.init({
  // Global settings:
  disable: 'tablet', // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
});
jQuery(document).ready(function(){
        jQuery(".search_btn_main").click(function(event){
        event.preventDefault();
        jQuery(".search_pop").addClass("search_pop_open");
    });
      jQuery(".close_btn").click(function(event){
           event.preventDefault();  
        jQuery(".search_pop").removeClass("search_pop_open");
    });
    
        /*    header height*/
//    var headerHeight = jQuery("header").outerHeight();
  //   jQuery('body').animate({"padding-top" : headerHeight }, 100);
 
    
    jQuery('.pop_up_main').click(function(){
        jQuery('pop_up_main').addClass('bounce');
    });
  
 jQuery('.product_sidebar_link .card .collapsed').click(function(){
var  self = jQuery(this);
      setTimeout(function(){
          jQuery('.product_sidebar_link .card .collapsed').removeClass('active');
           self.addClass('active');
      }, 100);
  });
    
    
    
var  lemg = jQuery("body").find('.product_detait_product ');
    
if (jQuery(lemg).length > 0) { 
 jQuery('.product_slider .slider').slick({
    slidesToShow: 1,
 	slidesToScroll: 1,
 	arrows: false,
 	fade: false,
 	asNavFor: '.product_slider .slider-nav-thumbnails',
     infinite: false, 
 }); 

 jQuery('.product_slider .slider-nav-thumbnails').slick({
 	slidesToShow: 3,
 	slidesToScroll: 1,
 	asNavFor: '.slider',
 	dots: false,  
 	focusOnSelect: true,
     infinite: true,
    arrows: false,
     centerMode:true,
     initialSlide:1
 });
}

});





var btn = jQuery('#top_arrow');

jQuery(window).scroll(function() {
  if (jQuery(window).scrollTop() > 1000) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});
 
btn.on('click', function(e) {
  e.preventDefault();
  jQuery('html, body').animate({scrollTop:0}, '300');
});



jQuery(function(){ 
    jQuery('.select_country .selectpicker').selectpicker();
});


/* custom pop up*/

/*video stop pop*/

jQuery(".video_pop").on("hidden.bs.modal", function() {

    var _this = this,
        youtubeSrc = jQuery(_this).find("iframe").attr("src");

    if(jQuery(_this).find("iframe").length > 0){                     // checking if there is iframe only then it will go to next level
        jQuery(_this).find("iframe").attr("src", "");                // removing src on runtime to stop video
        jQuery(_this).find("iframe").attr("src", youtubeSrc);        // again passing youtube src value to iframe
    }
});





jQuery('.firstCap').on('keypress', function(event) {
var $this = jQuery(this),
thisVal = $this.val(),
FLC = thisVal.slice(0, 1).toUpperCase();
con = thisVal.slice(1, thisVal.length);
jQuery(this).val(FLC + con);
});
 
 
jQuery(document).ready(function(){ 
     //var headerHeight = jQuery("header").outerHeight();
    //jQuery('body').animate({"padding-top": headerHeight}, 100);
    
    setTimeout(function(){
     jQuery('.fixed_contact .cont_btn.whatapp_btn ').addClass('bounceInLeft');
    }, 1500);
    setTimeout(function(){
     jQuery('.fixed_contact .cont_btn.call_btn ').addClass('bounceInLeft');
    }, 1800);
});  


jQuery(window).on('scroll', function(){
        var scroll = $(window).scrollTop();
//        console.log(scroll);
    if(scroll < 350){
              var headerHeight = jQuery("header").outerHeight();

        //jQuery('.banner_main').animate({"padding-top": headerHeight - 8}, 500);  
          jQuery('body').css("padding-top", headerHeight);    
 
    } 
});

jQuery(window).on('resize load ', function(){
      var headerHeight = jQuery("header").outerHeight();
    jQuery('body').animate({"padding-top": headerHeight}, 100);
 
}); 

 
jQuery(window).on("load resize", function (e) { 
	//mobileElements(); 
	jQuery('.mainloader').hide();

 
	}); 

  //jQuery('.play_btn').on('click', function(ev) {  	
  //var id = $(this).attr('data-rel');	
  //jQuery(".play-video"+id)[0].src += "&autoplay=1";
      
   // ev.preventDefault();
 
  //});
// jQuery(window).on("load" , function() {
     // jQuery("iframe")[0].src += "&autoplay=1";
    // ev.preventDefault();
 
// });
  
  
jQuery(document).ready(function(){
jQuery('p').each(function() {
var $this = jQuery(this);
if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
$this.remove();
});
});



	(function(jQuery){
			jQuery(window).on("load",function(){
				
				jQuery(".custom_scroll").mCustomScrollbar({
					theme:"minimal"
				});
				
			});
		})(jQuery);
