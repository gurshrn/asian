<?php 

get_header();
get_sidebar();

?>

  <?php $bannerImage = get_field('banner_image','options'); ?>

	<section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('banner_title','options');?></h1>

                 </div>

            </div>

    </section>
    <section class="blog_detail_main">

            <div class="container">

            	<?php
        					$post_id = get_the_id();
        					$my_post = get_post($post_id);
        					$post_date = $my_post->post_date;			
        					$post_content = $my_post->post_content;			
        					$post_title = $my_post->post_title;		
        					$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); 
					
				      ?>

                <div class="row">

                  <div class="col-lg-9 col-md-8">

                      <div class="blog_detail_lt about_text " data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                          <figure data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000" class="blog_detail_figure" style="background-image:url(<?php echo $image; ?>)"></figure>

                          <h3><?php echo $post_title;?></h3>

                          <p><strong><?php echo get_the_date('d M Y'); ?> || By John Smith</strong></p>

                          <p><?php echo $post_content;?></p>

                      </div>

                    </div>

                  <div class="col-lg-3 col-md-4">

                        <div class="blog_recent_rt">

                             <div class="search_bar_main">

                              <input type="text" class="form-control" placeholder="Search">

                                <button class="search_btn searchbar_btn"><i class="fa fa-search" aria-hidden="true"></i></button>

                            </div>

                            <div class="blog_recent_post">

                                <h3 class="wow fadeInDown  animated" data-wow-duration="1s" data-wow-delay="0.1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInDown;">Recent Posts</h3>

                                <ul>

                                  <?php
                                      $args = array(
                                          'post_type' => 'post',
                                          'posts_per_page' => 4,
                                      );

                                      $post_query = new WP_Query($args);
                                      if($post_query->have_posts() ) 
                                      {
                                        while($post_query->have_posts() ) 
                                        {
                                            $post_query->the_post();
                                  ?>

                                    <li class="wow fadeInDown  animated" data-wow-duration="1s" data-wow-delay="0.1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInDown;">

                                        <figure class="recent_post_figure" style="background-image: url(<?php echo the_post_thumbnail_url();?>)">

                                            <a href="<?php the_permalink(); ?>"></a>

                                        </figure>

                                       <div class="figure-cont">

                                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                            <p><?php substr(the_excerpt(), 0, 4); ?></p>

                                        </div>

                                    </li>

                                   <?php
                                        }
                                      }
                                  ?>
                              </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section> 
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
