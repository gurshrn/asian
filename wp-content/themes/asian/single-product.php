<?php 
	get_header();
	get_sidebar();

?>

	<?php $bannerImage = get_field('product_banner_image','options'); ?>

    <section class="banner banner_inn" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">

                <div class="banner_text" data-aos="slide-right" data-aos-easing="ease" data-aos-delay="400"  data-aos-duration="1000">

                      <h1><?php the_field('product_banner_title','options');?></h1>

                 </div>

            </div>

    </section>

       

<section class="product_com produt_detail_main">

    <div class="container">

        <div class="row">

          <div class="col-lg-3 col-md-4">

              <div class="product_detait_com product_detait_sidebar ">

                  <ul>

                    <?php
                      $args = array(
                        'post_type'   => 'product',
                        'post_status' => 'publish',
						'posts_per_page' => -1
                      );
                      $products = new WP_Query( $args );
                      if( $products->have_posts() ) :
                      ?>
                        
                          <?php
                            while( $products->have_posts() ) :
                              $products->the_post();
                            ?>
                                <li><a href="<?php the_permalink(); ?>"><?php echo  get_the_title(); ?></a></li>
                                
                              <?php
                            endwhile;
                            wp_reset_postdata();
                          ?>
                       
                      <?php endif; ?>
                    
                    </ul>

              </div>

            </div>

            <?php
				$post_id = get_the_id();
				$my_post = get_post($post_id);
				$post_date = $my_post->post_date;			
				$post_content = $my_post->post_content;			
				$post_title = $my_post->post_title;		
				$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') );
				$imageGallery = get_field('gallery'); 
		
	      	?>

          <div class="col-lg-9 col-md-8">

              <div class="product_detait_com product_detait_rt">

                  <div class="row product_detait_content product_slider">

                    <div class="col-lg-5 col-md-12">

                       <div class="product_detait_product">

                          	<div class="slider border_product">

                                <!-- <figure  class="product_detail_figure flex_bx">

                                    <img src="<?php echo $image; ?>" />

                                </figure> -->


                                <?php

                                    //echo "<pre>"; print_r($imageGallery);

                                    if( $imageGallery ): ?>

                                        <?php foreach( $imageGallery as $image ): ?>

                                              <figure  class="product_detail_figure flex_bx">

                                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['name']; ?>" />

                                              </figure>

                                        <?php endforeach; ?>

                                <?php endif; ?>

                            </div>

                           

                          <!-- slider thumbnails-->

                          <div class="slider-nav-thumbnails border_product">

                            <?php

                               

                                if( $imageGallery ): ?>

                                    <?php foreach( $imageGallery as $image ): ?>

                                          <div class="flex_bx slider_thumb"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['name']; ?>">
                                          </div>
                                           

                                    <?php endforeach; ?>

                            <?php endif; ?>
                        </div>
                        
                      </div>

                    </div>

                    <div class="col-lg-7 col-md-12">

                        <div class="product_detait_cont_rt about_text ">

                         	<h3><?php echo $post_title;?></h3>

                            <p><?php echo $post_content;?></p>

						</div>

                      </div>

                  </div>

              </div>

              

              <div class="product_detait_row">

                 <div class="contact_form product_get_touch">

                

                  <h2>Get In <strong>Touch</strong></h2>

                    <div class="contact_form_inn">

                      <?php echo do_shortcode( '[contact-form-7 id="325" title="Product Form"]' ); ?>
                    </div> 

                  

              </div>

              </div>

              <div class="product_detait_row">

                <div class="feature_product">

                <h2>Features <strong>Products</strong></h2>

                  <div class="row">
				  
						<?php
							$args = array(
							  'post_type'   => 'product',
							  'post_status' => 'publish',
							);
							$products = new WP_Query( $args );
							
							if( $products->have_posts() ) :
								
								while( $products->have_posts() ) :
								
									$products->the_post();
									
									$featuredImage = get_field('featured');
									
									if(isset($featuredImage) && $featuredImage[0] == 'Yes'):
										
						?>
  
									<div class="col-lg-4 col-md-6">

										<div class="our_project_bx  border_product">

											<figure class="product_figure" style="background-image:url(<?php echo the_post_thumbnail_url();?>)"><a href="<?php the_permalink(); ?>"></a></figure>

											<h3><a href="<?php the_permalink(); ?>"><?php echo  get_the_title(); ?></a></h3>

										</div>

									</div>
						 <?php
							endif; 
							endwhile;
							wp_reset_postdata();
							endif; 
						?>
						</div>

              </div>

              </div>

            </div>

        </div> 

    </div>

</section>  

    
<?php get_footer();?>

