<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<footer class="section">

        <div class="gear_icon gear_icon_rt"><img src="<?php echo get_template_directory_uri()?>/images/gear.gif" alt="gear.gif" /></div>

    		<div class="footer_top">

      			<div class="container"> 

          			<div class="row">

            			<div class="col-lg-3 col-md-3">

               				<div class="footer_bx footer_about">

               					<?php $footerImage = get_field('footer_logo','options');?>

                 				<a href="<?php echo get_site_url();?>" class="footer_logo"><img src="<?php echo $footerImage['url'];?>" alt="footer_logo.png" /></a>

              					<div class="footer_sm">

                     				<h3>Connect with Us</h3>

			                       <ul class="sm_icon">

			                         <li><a href="<?php the_field('facebook_link','options');?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

			                         <li><a href="<?php the_field('twitter_link','options');?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

			                         <li><a href="<?php the_field('linkedin_url','options');?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

			                       </ul>

                   				</div>
							</div>
						</div>

            			<div class="col-lg-5 col-md-5">

                			<div class="footer_bx footer_link">

                    			<h3>Quick Links</h3>

                    			<?php 
				                        $menu = wp_nav_menu( array( 'theme_location' => 'primary') ); 
				                ?>

			                    <!-- <ul>

			                      <li><a href="#">About Us</a></li>

			                      <li><a href="#">Products</a></li>

			                      <li><a href="#">Qc & Qa</a></li>

			                      <li><a href="#">Applications</a></li>

			                      <li><a href="#">Our Customers</a></li>

			                    </ul> -->

			                    <!-- <ul>

			                      <li><a href="#">Equipment</a></li>

			                      <li><a href="#">Technical Info</a></li>

			                      <li><a href="#">Articles</a></li>

			                      <li><a href="#">Blog</a></li>

			                      <li><a href="#">Contact Us</a></li>

			                    </ul> -->

                			</div>

            			</div>

			            <div class="col-lg-4 col-md-4">

			                 <div class="footer_bx footer_contact">

			                    <h3>Contact Us</h3>

			                     <ul>
			                     	<li><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><p><?php the_field('address','options');?></p></li>

			                       <li><span><i class="fa fa-phone" aria-hidden="true"></i></span><a href="tel:<?php the_field('phone_number','options');?>"><?php the_field('phone_number','options');?></a></li>

			                       <li><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span><a href="mailto:<?php the_field('email','options');?>"><?php the_field('email','options');?></a></li>

			                     </ul>

			                </div>

			            </div>

          			</div>

        		</div>

     		</div>

        	<div class="copyright_main">

          		<div class="container">

              		<div class="copyright_lt">
              			<p><?php the_field('copyright_left_text','options');?></p>
              		</div>

              		<div class="copyright_rt "><?php the_field('copyright_right_text','options');?></div>

          		</div>

    		</div>

    	</footer>

		<div class="modal fade pop_up_main video_pop animated" id="vedio_pop_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

  			<div class="modal-dialog" role="document">

			    <div class="modal-content">

			      <iframe src="https://www.youtube.com/embed/yAoLSRbwxL8" allow="autoplay; encrypted-media" allowfullscreen></iframe>

			        <a href="javascript:void(0);" class="close_btn " data-dismiss="modal">x</a>

			    </div>

  			</div>

		</div>

    	<div class="fixed_contact">

	        <a target="_blank" href="https://wa.me/<?php the_field('whatsapp_number','options');?>" class="cont_btn animated whatapp_btn"><img src="<?php echo get_template_directory_uri(); ?>/images/whatsapp.png" alt="whatsapp.png"/></a>

	        <a target="_blank" href="https://web.wechat.com/<?php the_field('calling_number','options');?>" class="cont_btn animated call_btn"><img src="<?php echo get_template_directory_uri(); ?>/images/call-answer.png" alt="call-answer.png"/></a>

    	</div>

    	<a href="javascript:void(0);" id="top_arrow"></a>

<?php wp_footer(); ?>

	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/popper.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-select.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/wow.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/aos.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
</body>
</html>

	
