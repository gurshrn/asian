<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<?php wp_head(); ?>
		<link rel="icon" href="<?php echo get_template_directory_uri().'/favicon.ico';?>" type="image/x-icon">
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"> 
		<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css"> 
	</head>
	<body>



	

